CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module allows taxonomy terms to be replaced by other terms.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/taxonomy_replace

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/taxonomy_replace


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Taxonomy Replace module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Taxonomy terms have a "Replace" tab, which allows users to select a
       taxonomy term in the same vocabulary.
    3. Any references to the current term will be replaced with a reference to
       the selected term, and the current term will be deleted.
    4. To replace terms, it is necessary to have the 'replace taxonomy terms'
       permission, and permission to delete the specified term.

MAINTAINERS
-----------

 * Malcolm Young (malcomio) - https://www.drupal.org/u/malcomio
