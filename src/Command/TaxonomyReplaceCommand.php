<?php

namespace Drupal\taxonomy_replace\Command;

use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy_replace\Service\TaxonomyReplaceService;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for taxonomy_replace.
 */
class TaxonomyReplaceCommand extends DrushCommands {

  /**
   * The TaxonomyReplaceService.
   *
   * @var \Drupal\taxonomy_replace\Service\TaxonomyReplaceService
   */
  protected $replaceService;

  /**
   * TaxonomyReplaceCommand constructor.
   *
   * @param \Drupal\taxonomy_replace\Service\TaxonomyReplaceService $replaceService
   *   The TaxonomyReplaceService.
   */
  public function __construct(TaxonomyReplaceService $replaceService) {
    parent::__construct();
    $this->replaceService = $replaceService;
  }

  /**
   * Replaces references to a taxonomy term with references to another term.
   *
   * @param int $oldTid
   *   The term ID for the old taxonomy term.
   * @param int $newTid
   *   The term ID for the new taxonomy term.
   * @param array $options
   *   Additional options for the command.
   *
   * @command taxonomy:replace
   *
   * @option delete
   *   Delete the old term after updating nodes.
   *
   * @usage taxonomy:replace 1 2
   * @usage taxonomy:replace 1 2 5 --delete
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function replace(int $oldTid, int $newTid, array $options = [
    'delete' => FALSE,
  ]) {
    /** @var \Drupal\taxonomy\Entity\Term $oldTerm */
    $oldTerm = Term::load($oldTid);
    $oldTermName = $oldTerm->label();

    /** @var \Drupal\taxonomy\Entity\Term $newTerm */
    $newTerm = Term::load($newTid);
    $newTermName = $newTerm->label();

    if ($oldTerm) {
      if ($newTerm) {
        $num_affected = $this->replaceService->countAffected($oldTerm);
        if ($num_affected == 0) {

          $message = t('There are no nodes with a reference for the term @label. Aborting.', [
            '@label' => $oldTermName,
          ]);
          $this->io()->error($message);
          return;
        }

        $message = t('Total nodes with reference to @oldTerm: @count. Do you wish to continue processing?', [
          '@oldTerm' => $oldTermName,
          '@count' => $num_affected,
        ]);
        $choice = $this->io()->confirm($message);
        if ($choice) {
          $nodes = $this->replaceService->replace($oldTerm, $newTerm, $limit);
          $this->io()
            ->success(t('@count nodes with reference to @oldTerm updated to reference @newTerm.', [
              '@count' => $nodes,
              '@oldTerm' => $oldTermName,
              '@newTerm' => $newTermName,
            ]));
        }

        if ($options['delete']) {
          $oldTerm->delete();
          $this->io()->success(t('@oldTerm has been deleted.', [
            '@oldTerm' => $oldTermName,
          ]));
        }
      }
      else {
        $message = t('Term with id @newTid does not exist.', [
          '@newTid' => $newTid,
        ]);
        $this->io()->error($message);
      }
    }
    else {
      $message = t('Term with id @oldTid does not exist.', [
        '@oldTid' => $newTid,
      ]);
      $this->io()->error($message);
    }
  }

}
