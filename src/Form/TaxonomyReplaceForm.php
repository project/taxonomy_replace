<?php

namespace Drupal\taxonomy_replace\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy_replace\Service\TaxonomyReplaceService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler class for taxonomy_replace.
 *
 * @package Drupal\taxonomy_replace\Form
 */
class TaxonomyReplaceForm extends ContentEntityDeleteForm {

  /**
   * The TaxonomyReplaceService.
   *
   * @var \Drupal\taxonomy_replace\Service\TaxonomyReplaceService
   */
  protected $replaceService;

  /**
   * TaxonomyReplaceCommand constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\taxonomy_replace\Service\TaxonomyReplaceService $replaceService
   *   The TaxonomyReplaceService.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, TaxonomyReplaceService $replaceService) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->time = $time;
    $this->replaceService = $replaceService;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('taxonomy_replace.replacer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_replace_term_replace_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\taxonomy\Entity\Term $term */
    $term = $this->getEntity();
    $old_tid = $term->id();
    $vid = $term->bundle();

    $form['old_term'] = [
      '#title' => $this->t('Current taxonomy term'),
      '#type' => 'textfield',
      '#value' => $term->label() . " ($old_tid)",
      '#disabled' => TRUE,
    ];

    $form['old_tid'] = [
      '#type' => 'value',
      '#value' => $old_tid,
    ];

    $nodes = $this->replaceService->getNidsByTid($old_tid);
    $node_list = [];
    foreach ($nodes as $row) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($row->nid);
      $node_list[] = $node->toLink();
    }

    $form['nodes'] = [
      '#type' => 'details',
      '#title' => $this->t('%nodes nodes will be updated', [
        '%nodes' => count($node_list)
      ]),
    ];

    $form['nodes']['list'] = [
      '#theme' => 'item_list',
      '#items' => $node_list,
    ];

    $form['new_tid'] = [
      '#title' => $this->t('Taxonomy term to use instead'),
      '#type' => 'entity_autocomplete',
      '#required' => TRUE,
      '#target_type' => 'taxonomy_term',
      // Limit the selection to the same vocabulary.
      '#selection_settings' => [
        'target_bundles' => [
          $vid => $vid,
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $new_tid = $form_state->getValue('new_tid');

    /** @var \Drupal\taxonomy\Entity\Term $old_term */
    $old_term = $this->getEntity();
    /** @var \Drupal\taxonomy\Entity\Term $new_term */
    $new_term = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->load($new_tid);

    $count = $this->replaceService->replace($old_term, $new_term);

    $tokens = [
      '%nodes' => $count,
      '%old_term' => $this->entity->label(),
      '%new_term' => $new_term->label(),
    ];
    $this->messenger()->addStatus($this->t('%nodes references to %old_term have been replaced by references to %new_term', $tokens));

    // Delete the old term.
    parent::submitForm($form, $form_state);

    // Redirect to the new taxonomy term.
    $form_state->setRedirectUrl(new Url('entity.taxonomy_term.canonical', [
      'taxonomy_term' => $new_tid,
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to replace and delete the @entity-type %label?', [
      '@entity-type' => $this->getEntity()
        ->getEntityType()
        ->getSingularLabel(),
      '%label' => $this->getEntity()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.taxonomy_vocabulary.collection');
  }

}
