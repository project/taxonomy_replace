<?php

namespace Drupal\taxonomy_replace\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Class containing TaxonomyReplaceService class.
 */
class TaxonomyReplaceService {

  /**
   * The type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $typeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * TaxonomyReplaceService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $manager, Connection $database) {
    $this->typeManager = $manager;
    $this->database = $database;
  }

  /**
   * Get the number of nodes relating to a term.
   *
   * @param \Drupal\taxonomy\Entity\Term $oldTerm
   *   The existing taxonomy term.
   *
   * @return int
   *   The number of related nodes.
   */
  public function countAffected(Term $oldTerm) {
    $query = $this->database->select('taxonomy_index', 'ti');
    $query->fields('ti', ['nid', 'tid']);
    $query->condition('ti.tid', $oldTerm->id());
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Replace term references on nodes.
   *
   * @param \Drupal\taxonomy\Entity\Term $oldTerm
   *   The old taxonomy term.
   * @param \Drupal\taxonomy\Entity\Term $newTerm
   *   The new taxonomy term.
   *
   * @return int
   *   The number of nodes updated.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function replace(Term $oldTerm, Term $newTerm) {

    $count = 0;

    $nidsTids = $this->getNidsByTid($oldTerm->id());
    foreach ($nidsTids as $row) {
      /** @var \Drupal\node\Entity\Node $node */
      $node = Node::load($row->nid);
      $field_name = $this->getTermReferenceField($node, $oldTerm);

      // Add a reference to the new term, unless there is one already.
      if (!$this->hasReference($node, $field_name, $newTerm->id())) {
        $node->{$field_name}->appendItem($newTerm);
      }

      // Remove the old term reference.
      $node_term_list = $node->{$field_name};
      $terms_on_node = $node_term_list->referencedEntities();
      foreach ($terms_on_node as $key => $term) {
        if ($term->id() == $oldTerm->id()) {
          $node->{$field_name}->removeItem($key);
        }
      }
      $node->save();
      $count++;
    }

    $tokens = [
      '%old_term' => $oldTerm->label(),
      '%new_term' => $newTerm->label(),
    ];
    \Drupal::logger('TaxonomyReplaceService')
      ->info('References to %old_term have been replaced by references to %new_term', $tokens);

    return $count;
  }

  /**
   * Get all nodes with a specific term ID.
   *
   * @param int $term_id
   *   The term ID to search for.
   *
   * @return mixed
   *   Array of nid and tid
   */
  public function getNidsByTid(int $term_id) {
    $query = $this->database->select('taxonomy_index', 'ti');
    $query->fields('ti', ['nid', 'tid']);
    $query->join('node', 'n', 'n.nid = ti.nid');
    $query->condition('ti.tid', $term_id);
    return $query->execute()->fetchAll();
  }

  /**
   * Get the name of the term reference field relevant to this term ID.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node holding the term reference field.
   * @param \Drupal\taxonomy\Entity\Term $term
   *   The term being referenced.
   *
   * @return string
   *   The field name.
   */
  protected function getTermReferenceField(Node $node, Term $term) {
    $field_name = '';

    $defs = $node->getFieldDefinitions();

    foreach ($defs as $field) {
      if ($field->getType() == 'entity_reference') {
        $settings = $field->getSettings();
        if ($settings['target_type'] == 'taxonomy_term') {
          $field_name = $field->getName();
          $referenced_terms = $node->{$field_name}->referencedEntities();
          if (in_array($term, $referenced_terms)) {
            return $field_name;
          }
        }
      }
    }

    return $field_name;
  }

  /**
   * Check if a node field has a particular term reference.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object.
   * @param string $field_name
   *   The field name.
   * @param int $tid
   *   The term ID.
   *
   * @return bool
   *   TRUE if the field contains a reference to that term.
   */
  private function hasReference(Node $node, string $field_name, int $tid) {
    // @todo is there a more efficient way to do this, or a core function?
    $has_reference = FALSE;
    $node_term_list = $node->{$field_name};
    $terms_on_node = $node_term_list->referencedEntities();
    foreach ($terms_on_node as $key => $term) {
      if ($term->id() == $tid) {
        $has_reference = TRUE;
        break;
      }
    }

    return $has_reference;
  }

}
